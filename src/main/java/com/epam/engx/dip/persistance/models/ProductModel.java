package com.epam.engx.dip.persistance.models;

import java.util.List;

public class ProductModel {
	private int id;
	private String productName;
	private String productDescription;
	private double productPrice;
	private String categoryName;
	private double productRating;
	private List<String> productReviews;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getProductDescription() {
		return productDescription;
	}
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}
	public double getProductPrice() {
		return productPrice;
	}
	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public double getProductRating() {
		return productRating;
	}
	public void setProductRating(double productRating) {
		this.productRating = productRating;
	}
	public List<String> getProductReviews() {
		return productReviews;
	}
	public void setProductReviews(List<String> productReviews) {
		this.productReviews = productReviews;
	}
	
}
